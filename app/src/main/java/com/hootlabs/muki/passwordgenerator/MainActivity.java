package com.hootlabs.muki.passwordgenerator;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onEditTextClick(View view){
        EditText length = (EditText) findViewById(R.id.numOfChars);
        length.selectAll();
    }
    public void onGenPassClick(View view){
        String regexNum = "^[0-9]+$";
        String password;
        int numOfChar;
        Boolean isNumeric;


        TextView passwordText = (TextView) findViewById(R.id.passwordTextView);
        EditText length = (EditText) findViewById(R.id.numOfChars);
        CheckBox upper = (CheckBox) findViewById(R.id.checkBoxUpper);
        CheckBox lower = (CheckBox) findViewById(R.id.checkBoxLower);
        CheckBox digit = (CheckBox) findViewById(R.id.checkBoxDigits);
        CheckBox special = (CheckBox) findViewById(R.id.checkBoxSpecial);

        if (view != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        if (!upper.isChecked() && !lower.isChecked() && !digit.isChecked() && !special.isChecked()){
            upper.setChecked(true);
        }

        isNumeric = length.getText().toString().trim().matches(regexNum);

        if (isNumeric){
            numOfChar = Integer.parseInt(length.getText().toString());
            if (numOfChar < 8 || numOfChar > 256){
                numOfChar = 8;
                length.setText("8");
            }
        }
        else{
            length.setText("8");
            numOfChar = 8;
        }

        password = generatePassword(numOfChar, upper.isChecked(), lower.isChecked(), digit.isChecked(), special.isChecked());

        passwordText.setText(password);
    }

    private static String generatePassword(int len, boolean upper, boolean lower, boolean digit, boolean special){
        String allChars = "";
        String passwd = "";
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerCase = upperCase.toLowerCase();
        String digitChar = "0123456789";
        String specialChar = "!@#$%^&*";
        ArrayList<String> password = new ArrayList<>();

        SecureRandom rnd = new SecureRandom ();
        int rndInt;

        if (upper) {
            password.add("U");
            allChars += upperCase;
        }
        if (lower) {
            password.add("L");
            allChars += lowerCase;
        }
        if (digit) {
            password.add("D");
            allChars += digitChar;
        }
        if (special) {
            password.add("S");
            allChars += specialChar;
        }

        while (password.size() < len)
            password.add("A");

        Collections.shuffle(password);

        for (int i = 0; i < password.size(); i++){
            switch(password.get(i)){
                case "U":
                    rndInt = rnd.nextInt(upperCase.length());
                    passwd += upperCase.substring(rndInt, rndInt+1);
                    break;
                case "L":
                    rndInt = rnd.nextInt(lowerCase.length());
                    passwd += lowerCase.substring(rndInt, rndInt+1);
                    break;
                case "D":
                    rndInt = rnd.nextInt(digitChar.length());
                    passwd += digitChar.substring(rndInt, rndInt+1);
                    break;
                case "S":
                    rndInt = rnd.nextInt(specialChar.length());
                    passwd += specialChar.substring(rndInt, rndInt + 1);
                    break;
                case "A":
                    rndInt = rnd.nextInt(allChars.length());
                    passwd += allChars.substring(rndInt, rndInt+1);
                    break;
            }
        }

        return passwd;
    }
}